# @ayanaware/eslint-plugin

## Installation

Install via yarn or npm:
```bash
yarn add --dev @ayanaware/eslint-plugin

npm i --save-dev @ayanaware/eslint-plugin
```

## Configuration

Put the `.eslintrc.json` file into the root folder of your project.
Some default environments may be given, but they should be configured according to https://eslint.org/docs/user-guide/configuring#specifying-environments

### For JavaScript linting

`.eslintrc.json`:
```json
{
    "extends": [
        "plugin:@ayanaware/javascript"
    ],
    "env": {

	},
	"ignorePatterns": [
		"node_modules/",
		"dist/",
		"build/"
	]
}
```

### For TypeScript linting

`.eslintrc.json`:
```json
{
    "extends": [
        "plugin:@ayanaware/typescript"
    ],
    "env": {
		"es2020": true,
	},
	"ignorePatterns": [
		"node_modules/",
		"dist/",
		"build/"
	]
}
```
