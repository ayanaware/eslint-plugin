/**
 * Linting rules for TypeScript.
 *
 * @see https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/ROADMAP.md
 */
module.exports = {
	extends: [
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:@typescript-eslint/recommended-requiring-type-checking',
	],
	plugins: [
		'@typescript-eslint',
	],
	rules: {
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/array-type.md
		'@typescript-eslint/array-type': [
			'error',
			{ default: 'generic' },
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/require-await.md
		'@typescript-eslint/require-await': 'off',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/comma-dangle.md
		'@typescript-eslint/comma-dangle': [
			'error',
			'always-multiline',
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/comma-spacing.md
		'@typescript-eslint/comma-spacing': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/consistent-type-assertions.md
		'@typescript-eslint/consistent-type-assertions': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/consistent-type-definitions.md
		'@typescript-eslint/consistent-type-definitions': [
			'error',
			'interface',
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/dot-notation.md
		'@typescript-eslint/dot-notation': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/explicit-module-boundary-types.md
		'@typescript-eslint/explicit-module-boundary-types': 'warn',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/indent.md

		// Disabled due to: https://github.com/typescript-eslint/typescript-eslint/issues/1824
		'@typescript-eslint/indent': 'off',

		// '@typescript-eslint/indent': [
		// 	'error',
		// 	'tab',
		// 	{
		// 		SwitchCase: 1,
		// 		flatTernaryExpressions: true,
		// 		FunctionDeclaration: {
		// 			parameters: 'first',
		// 		},
		// 		FunctionExpression: {
		// 			parameters: 'first',
		// 		},
		// 	},
		// ],

		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/keyword-spacing.md
		'@typescript-eslint/keyword-spacing': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/lines-between-class-members.md
		'@typescript-eslint/lines-between-class-members': [
			'error',
			'always',
			{
				exceptAfterSingleLine: true,
				exceptAfterOverload: true,
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-delimiter-style.md
		'@typescript-eslint/member-delimiter-style': [
			'error',
			{
				overrides: {
					interface: {
						multiline: {
							delimiter: 'semi',
							requireLast: true,
						},
						singleline: {
							delimiter: 'semi',
							requireLast: false,
						},
					},
					typeLiteral: {
						multiline: {
							delimiter: 'comma',
							requireLast: true,
						},
						singleline: {
							delimiter: 'comma',
							requireLast: false,
						},
					},
				},
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/member-ordering.md
		'@typescript-eslint/member-ordering': [
			'error',
			{
				default: [
					'signature',

					'static-field',
					'instance-field',
					'abstract-field',

					'constructor',

					'static-method',
					'instance-method',
					'abstract-method',
				],
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/naming-convention.md
		'@typescript-eslint/naming-convention': [
			'error',
			// GROUPS
			{
				selector: 'default',
				format: ['camelCase'],
				leadingUnderscore: 'allow',
				trailingUnderscore: 'forbid',
			},

			{
				selector: 'variableLike', // variable, function, parameter
				format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
			},

			{
				selector: 'memberLike', // property, parameterProperty, method, accessor, enumMember
				format: ['camelCase', 'UPPER_CASE'],
			},

			{
				selector: 'typeLike', // class, interface, typeAlias, enum, typeParameter
				format: ['PascalCase'],
			},

			// Overrides
			// Allow myFunction, MyFunction, MY_FUNCTION
			{
				selector: 'function',
				format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
			},

			// Enum Members should be UPPER_CASE, but also allow PascalCase
			{
				selector: 'enumMember',
				format: ['UPPER_CASE', 'PascalCase'],
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-empty-function.md
		'@typescript-eslint/no-empty-function': 'warn',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-empty-interface.md
		'@typescript-eslint/no-empty-interface': 'warn',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-explicit-any.md
		'@typescript-eslint/no-explicit-any': 'off',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-floating-promises.md
		'@typescript-eslint/no-floating-promises': [
			'error',
			{
				ignoreVoid: true,
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-inferrable-types.md
		'@typescript-eslint/no-inferrable-types': 'off',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-non-null-assertion.md
		'@typescript-eslint/no-non-null-assertion': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-redeclare.md
		// Rule should remain disabled. Reason: https://github.com/typescript-eslint/typescript-eslint/issues/2585#issuecomment-696331548
		'@typescript-eslint/no-redeclare': 'off',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-shadow.md
		'@typescript-eslint/no-shadow': [
			'error',
			{
				hoist: 'all',
				ignoreTypeValueShadow: true,
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unnecessary-boolean-literal-compare.md
		'@typescript-eslint/no-unnecessary-boolean-literal-compare': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-expressions.md
		'@typescript-eslint/no-unused-expressions': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-unused-vars.md
		'@typescript-eslint/no-unused-vars': 'off',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-for-of.md
		'@typescript-eslint/prefer-for-of': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-function-type.md
		'@typescript-eslint/prefer-function-type': 'error',
		// TODO: Consider enabling this rule (is the compiler handling this properly when going to ES5?)
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-includes.md
		'@typescript-eslint/prefer-includes': 'off',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-readonly.md
		'@typescript-eslint/prefer-readonly': 'error',
		// TODO: Consider enabling this rule (is the compiler handling this properly when going to ES5?)
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/prefer-string-starts-ends-with.md
		'@typescript-eslint/prefer-string-starts-ends-with': 'off',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/promise-function-async.md
		'@typescript-eslint/promise-function-async': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/quotes.md
		'@typescript-eslint/quotes': [
			'error',
			'single',
		],
		// TODO: This rule seems buggy, eventually enable it later
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/restrict-template-expressions.md
		'@typescript-eslint/restrict-template-expressions': [
			'off',
			{
				allowNumber: true,
				allowBoolean: true,
				allowAny: true,
				allowNullable: false,
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/semi.md
		'@typescript-eslint/semi': [
			'error',
			'always',
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/space-before-function-paren.md
		'@typescript-eslint/space-before-function-paren': [
			'error',
			{
				anonymous: 'never',
				named: 'never',
				asyncArrow: 'always',
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/type-annotation-spacing.md
		'@typescript-eslint/type-annotation-spacing': 'error',
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/unbound-method.md
		'@typescript-eslint/unbound-method': [
			'error',
			{
				ignoreStatic: true,
			},
		],
		// https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/unified-signatures.md
		'@typescript-eslint/unified-signatures': 'error',
	},
};
