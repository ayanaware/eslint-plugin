const typescriptParser = require('../../parser/typescript');

const base = './configs/typescript';

/**
 * Configuration for linting TypeScript.
 * This is also includes the JavaScript base configuration.
 *
 * Configuration name: plugin:@ayanaware/typescript
 */
module.exports = {
	extends: [
		'plugin:@ayanaware/javascript',
	],
	overrides: [
		{
			files: [
				'*.ts',
				'*.tsx',
			],
			...typescriptParser,
			extends: [
				// Paths here need to be relative to the projects root directory!
				`${base}/eslint.js`,
				`${base}/typescript-eslint.js`,
				`${base}/eslint-plugin-deprecation.js`,
				`${base}/eslint-plugin-import.js`,
				`${base}/ayanaware-eslint.js`,
			],
		},
	],
};
