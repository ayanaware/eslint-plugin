const typescriptImportParser = require('../../parser/typescript-import');

/**
 * Rules for linting deprecation in TypeScript.
 *
 * @see https://github.com/gund/eslint-plugin-deprecation
 */
module.exports = {
	...typescriptImportParser,
	plugins: [
		'deprecation',
	],
	rules: {
		// https://github.com/gund/eslint-plugin-deprecation
		'deprecation/deprecation': 'warn',
	},
};
