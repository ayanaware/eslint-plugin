/**
 * ESLint rules from this package
 */
module.exports = {
	plugins: [
		'@ayanaware',
	],
	rules: {
		'@ayanaware/import-declaration-multiline': [
			'error',
			'as-needed',
			{
				maxLengthSingelineImport: 140,
			},
		],
	},
};
