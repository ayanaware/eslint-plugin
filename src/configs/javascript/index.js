const base = './configs/javascript';

/**
 * Configuration for linting JavaScript.
 * This is also used inside the TypeScript configuration as a base.
 *
 * Configuration name: plugin:@ayanaware/javascript
 */
module.exports = {
	overrides: [
		{
			files: [
				'*.js',
				'*.jsx',
				'*.cjs',
				'*.mjs',
				'*.ts',
				'*.tsx',
			],
			extends: [
				// Paths here need to be relative to the projects root directory!
				`${base}/eslint.js`,
			],
		},
	],
};
