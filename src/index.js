// ESLint plugin main file
module.exports = {
	rules: require('./rules'),
	configs: require('./configs'),
};
