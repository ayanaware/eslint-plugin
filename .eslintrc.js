module.exports = {
	extends: './src/configs/javascript/eslint.js',
	env: {
		es2020: true,
		node: true,
	},
};
